﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TesT01
{

    public partial class Form1 : Form
    {      
        public Form1()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        private void btnThem_Click(object sender, EventArgs e)
        {
            
            string sqlthem = "INSERT INTO tblHotel VALUES (@maks, @nameks, @price, @tinhtrang)";
            SqlCommand cmd = new SqlCommand(sqlthem, conn);
            cmd.Parameters.AddWithValue("maks", txtMaKS.Text);
            cmd.Parameters.AddWithValue("nameks", txtNameKS.Text);
            cmd.Parameters.AddWithValue("price", txtPrice.Text);
            cmd.Parameters.AddWithValue("tinhtrang", cbTinhTrang.Text);
            cmd.ExecuteNonQuery();
            input();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            conn = new SqlConnection("Data Source=DESKTOP-4NVNJ2G;Initial Catalog=KhachSan;Integrated Security=TrueData Source=DESKTOP-4NVNJ2G;Initial Catalog=KhachSan;Integrated Security=True");
            conn.Open();
            input();
        }
        public void input()
        {
            string sqlInsert = "select MaKhachSan = maks, TenKhachSan =  nameks,Gia = price from tblHotel ";
            SqlCommand cmd = new SqlCommand(sqlInsert, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            tblHotel.DataSource = dt;
        }       

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            conn.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sqlxoa = "DELETE FROM tblHotel WHERE maks = @maks";
            SqlCommand cmd = new SqlCommand(sqlxoa, conn);
            cmd.Parameters.AddWithValue("maks", txtMaKS.Text);
            cmd.Parameters.AddWithValue("nameks", txtNameKS.Text);
            cmd.Parameters.AddWithValue("price", txtPrice.Text);
            cmd.Parameters.AddWithValue("tinhtrang", cbTinhTrang.Text);
            cmd.ExecuteNonQuery();
            input();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sqlsua = "UPDATE tblHotel SET maks = @maks, nameks = @nameks, price = @price, tinhtrang = @tinhtrang Where maks = @maks";
            SqlCommand cmd = new SqlCommand(sqlsua, conn);
            cmd.Parameters.AddWithValue("maks", txtMaKS.Text);
            cmd.Parameters.AddWithValue("nameks", txtNameKS.Text);
            cmd.Parameters.AddWithValue("price", txtPrice.Text);
            cmd.Parameters.AddWithValue("tinhtrang", cbTinhTrang.Text);
            cmd.ExecuteNonQuery();
            input();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string sqltim = "SELECT * FROM tblHotel WHERE maks = @maks";
            SqlCommand cmd = new SqlCommand(sqltim, conn);
            cmd.Parameters.AddWithValue("maks", txtMaKS.Text);
            cmd.Parameters.AddWithValue("nameks", txtNameKS.Text);
            cmd.Parameters.AddWithValue("price", txtPrice.Text);
            cmd.Parameters.AddWithValue("tinhtrang", cbTinhTrang.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            tblHotel.DataSource = dt;
        }

        private void bntTimKiem_Click(object sender, EventArgs e)
        {
            string sqltimkiem = "SELECT * FROM tblHotel WHERE nameks = @nameks";
            SqlCommand cmd = new SqlCommand(sqltimkiem, conn);
            cmd.Parameters.AddWithValue("maks", txtMaKS.Text);
            cmd.Parameters.AddWithValue("nameks", txtNameKS.Text);
            cmd.Parameters.AddWithValue("price", txtPrice.Text);
            cmd.Parameters.AddWithValue("tinhtrang", cbTinhTrang.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            tblHotel.DataSource = dt;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMaKS.Text = "";
            txtNameKS.Text = "";
            txtPrice.Text = "";
            cbTinhTrang.Text = "";
        }
    }
}
